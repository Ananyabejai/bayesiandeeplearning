# Deep Bayesian Active Learning on Images

<p>Labelled Data is expensive but is a basic requirement for any supervised machine learning task. With growing need for labelled data, techniques to reduce data demand is essential.
Active Learning helps to choose the data to be labelled which contributes maximum to the learning.

The unique idea of using dropout during testing the model led to Deep Bayesian Active Learning which forms the core of the paper <b>Deep Bayesian Active Learning with Image Data</b> by Yarin Gal et.al [[1]](#1). Active learning in a deep learning setup was discussed to overcome the dependance of deep learning models on large amount of data. Different sampling strategies to select informative samples in a active learning setup for a simple classification problem to a multi-class classification problem and its impact is investigated.

The code is tweaked from the implementation of Deep Bayesian Active Learning on Images [[2]](#2) but integrated with <b>ModAL</b> framework [[3]](#3).</p>

## References
1.  <a id="1">[Deep Bayesian Active Learning on Images, 2017](https://arxiv.org/abs/1703.02910)</a>
2. <a id="2">https://github.com/Riashat/Active-Learning-Bayesian-Convolutional-Neural-Networks</a>
3. <a id="3">https://github.com/modAL-python/modAL</a> 