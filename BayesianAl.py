from torchvision import models, transforms , utils
import torch
import numpy as np
import os
import pathlib
import PIL
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from skorch import NeuralNetClassifier
from modAL.models import ActiveLearner
from scipy.stats import mode
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--train_dir", help="Training Data", type=pathlib.Path)
parser.add_argument("--test_dir", help="Test Data", type=pathlib.Path)
parser.add_argument("--log_dir", help="Logging Directory", type=pathlib.Path)
parser.add_argument("--res_dir", help="Path to save results", type=str)#To save result as string
parser.add_argument("--seed", help="Random Seed", type=int)
parser.add_argument("--expno", help="Number of Experiment", type=int)
parser.add_argument("--gpu_id", help="CUDA Device ID", type=int)
parser.add_argument("--model", help="ResNet or Checkbox", type=str)
parser.add_argument("--sampling", help="Sampling Strategy- either Random, Entropy, Varratio, Std, Bald", type=str)
args = parser.parse_args()

print(args)

seed = args.seed
expno = args.expno
training_data = args.train_dir
test_data = args.test_dir
log_dir = args.log_dir
cuda_device = args.gpu_id
fpath= args.res_dir
modelname= args.model
sampling=args.sampling
np.random.seed(seed=seed)
torch.manual_seed(seed)


batch_size = 8
num_workers = 0
shuffle = True
lr = 3e-4

device = torch.device("cuda")
mean = [0.8372, 0.8372, 0.8375]
std = [0.2755, 0.2757, 0.2748]
size = (64,64)




class CheckBoxClassifier(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.feature_extractor = torch.nn.Sequential(torch.nn.Conv2d(in_channels=3,
                                                                     out_channels=8,
                                                                     kernel_size=3,
                                                                     stride=2,
                                                                     padding=1,
                                                                     bias=True),
                                                     torch.nn.ReLU(inplace=True),
                                                     torch.nn.Conv2d(in_channels=8,
                                                                     out_channels=64,
                                                                     kernel_size=3,
                                                                     stride=2,
                                                                     padding=1,
                                                                     bias=True),
                                                     torch.nn.ReLU(inplace=True))
                                                    
                                                    
        
                                                     
        self.max_pool = torch.nn.MaxPool2d(kernel_size=2)
        self.flatten = torch.nn.Flatten()
        self.dropout1 = torch.nn.Dropout(p=0.5)

        self.classifier = torch.nn.Sequential(torch.nn.Linear(4096,256),
                                              torch.nn.ReLU(inplace=True),
                                              torch.nn.Dropout(p=0.5),
                                              torch.nn.Linear(256,3),
                                              torch.nn.Softmax(dim=1))
        
    def forward(self,x):
        x = self.feature_extractor(x)
        x = self.max_pool(x)
        x = self.flatten(x)
        x = self.dropout1(x)
        x = self.classifier(x)
        return x 

original_model = models.resnet50(pretrained=True)
class Resnet(torch.nn.Module):
    def __init__(self):
        super(Resnet, self).__init__()
        self.feature_extractor = torch.nn.Sequential(*list(original_model.children())[:-1])
        
        self.classifier= torch.nn.Sequential(torch.nn.Dropout(p=0.7),
                                              torch.nn.Linear(2048,256),
                                              torch.nn.ReLU(inplace=True),
                                              torch.nn.Dropout(p=0.7),
                                              torch.nn.Linear(256,3),
                                              torch.nn.Softmax(dim=1))
       
                
    def forward(self, x):
        x = self.feature_extractor(x)
        x=x.view(-1,2048)
        x = self.classifier(x)
        return x 
        
if modelname == "Checkbox":
    model = CheckBoxClassifier()
    mepochs= 100
    subsetsize=1000
    
elif modelname == "ResNet":
    model = Resnet()
    mepochs= 10
    subsetsize=300
else:
    raise NotImplementedError(f" {model} is not implemented")
 

 
   
def active_learning_procedure(query_strategy,
                              X_test,
                              y_test,
                              X_pool,
                              y_pool,
                              X_initial,
                              y_initial,
                              estimator,
                              n_queries=100,
                              n_instances=10):
    
    
    learner = ActiveLearner(estimator=estimator,
                            X_training=X_initial,
                            y_training=y_initial,
                            query_strategy=query_strategy,
                           )
    perf_hist = [learner.score(X_test, y_test)]
    #times_per_iteration = []
    
    for index in range(n_queries):
        query_idx, query_instance = learner.query(X_pool, n_instances)
        learner.teach(X_pool[query_idx], y_pool[query_idx])
        X_pool = np.delete(X_pool, query_idx, axis=0)
        y_pool = np.delete(y_pool, query_idx, axis=0)
        model_accuracy = learner.score(X_test, y_test)
        #if (index% 10==0):
        print('Accuracy after query {n}: {acc:0.4f}'.format(n=index + 1, acc=model_accuracy))
        perf_hist.append(model_accuracy)
     
        
    return perf_hist,learner
    
def a():  
    def uniform(learner, X, n_instances=1): #n_instance is no of informative samples selected at a time
        query_idx = np.random.choice(range(len(X)), size=10, replace=False)
        return query_idx, X[query_idx]
    return uniform
def b():
    def max_entropy(learner, X, n_instances=1, T=100):
        random_subset = np.random.choice(range(len(X)), size=subsetsize, replace=False)
        All_preds = np.zeros(shape=(random_subset.shape[0], 3))
    
        with torch.no_grad():
            for t in range(100):
                preds = learner.estimator.forward(X[random_subset], training=True).cpu().numpy()
                All_preds = All_preds + preds    #all predictions summed for 100 iterations 
    
        Avg_Pred = np.divide(All_preds,100)     #average over 100 forward passes
        epsilon = np.finfo(np.float).eps 
        Entropy = (- Avg_Pred *np.log(Avg_Pred + epsilon)).sum(axis=1) #Calculate entropy and sum probabilities for each sample
        idx = (Entropy).argsort()[-n_instances:][::-1]
        query_idx = random_subset[idx]
        return query_idx, X[query_idx]
    return max_entropy
    
def c():
    def bald(learner, X, n_instances=1, T=100):
        random_subset = np.random.choice(range(len(X)), size=subsetsize, replace=False)
        All_preds = np.zeros(shape=(random_subset.shape[0], 3))
        All_Entropy_Dropout = np.zeros(shape=random_subset.shape[0])
        with torch.no_grad():
            for t in range(100):
                preds = learner.estimator.forward(X[random_subset], training=True).cpu().numpy()
                All_preds = All_preds + preds
                epsilon = np.finfo(np.float).eps 
                Entropy_per_dropout = -(np.multiply(preds,np.log(preds + epsilon))).sum(axis=1)
                All_Entropy_Dropout = All_Entropy_Dropout + Entropy_per_dropout
            #calculate entropy per dropout before taking average of predictions
            
   
        Avg_Pred = np.divide(All_preds,100)     #average over 100 forward passes
        epsilon = np.finfo(np.float).eps 
        Entropy_Average = - (np.multiply(Avg_Pred,np.log(Avg_Pred + epsilon))).sum(axis=1)
  

        Exp_Entropy = np.divide(All_Entropy_Dropout,100)
        Gain = Entropy_Average - Exp_Entropy
    
        a_1d = Gain.flatten()
        idx = a_1d.argsort()[-n_instances:][::-1]
        query_idx = random_subset[idx]
        return query_idx, X[query_idx]
    return bald    
  
def d():
    def var_ratio(learner, X, n_instances=1, T=100):
        random_subset = np.random.choice(range(len(X)), size=subsetsize, replace=False)
        All_Labels = np.zeros(shape=(random_subset.shape[0],1))
    
        with torch.no_grad():
            for t in range(100):
                probs = learner.estimator.forward(X[random_subset], training=True)
                _, predicted = torch.max(probs.data, 1) #get class label
                predicted=predicted.numpy()
                Labels = np.array([predicted]).T
                All_Labels = np.append(All_Labels, Labels, axis=1)# append class labels based on iteration columnwise
            
            Variation = np.zeros(shape=(random_subset.shape[0]))

            for r in range(random_subset.shape[0]): #samples
                L = np.array([0])
                for dropout in range(100): # no of dropouts
                    L = np.append(L, All_Labels[r, dropout+1]) # class labels for a sample in a row
                
                Predicted_Class, Mode = mode(L[1:]) # calculate the mode
                v = np.array(  [1 - Mode/float(100)])# variation=(1-mode/dropout iterations)
                Variation[t] = v
            

            a_1d = Variation.flatten()
            idx = a_1d.argsort()[-n_instances:][::-1]# samples with high variation ratio
            query_idx = random_subset[idx]
        return query_idx, X[query_idx]
    return var_ratio    

def e():
    def std(learner, X, n_instances=1, T=100):
        random_subset = np.random.choice(range(len(X)), size=subsetsize, replace=False)
        All_preds = np.zeros(shape=(random_subset.shape[0], 3))
        with torch.no_grad():
            for t in range(100):
                preds = learner.estimator.forward(X[random_subset], training=True).cpu().numpy()
            
                All_preds = np.append(All_preds, preds, axis=1)#prob for all 100 iterations for 3 classes 
            #Appended along the column, for each sample probabilities are in the same row.

            All_Std = np.zeros(shape=(random_subset.shape[0],3))
    
            Sigma = np.zeros(shape=(random_subset.shape[0],1))
    


            for t in range(random_subset.shape[0]):#samples considered
                for r in range(3):# 3 classes 
                    L = np.array([0])#initialize
                    for d_iter in range(100):
                        L = np.append(L, All_preds[t, r+3])#prob for each sample per iteration and class

                    L_std = np.std(L[1:])#standard deviation per class
                    All_Std[t,r] = L_std#std dev for all 3 classes
                    E = All_Std[t,:] # std dev where t is for sample  with all 3 columns of classes selected.
                    Sigma[t,0] = sum(E)# std deviation per sample
        
            a_1d = Sigma.flatten()
            idx = a_1d.argsort()[-n_instances:][::-1]#select last 10 instances(high deviation values)
            query_idx = random_subset[idx]
        return query_idx, X[query_idx]
    return std    
                               
if sampling == "Random":
    strategy = a()
elif sampling == "Entropy":
    strategy = b()
elif sampling == "Bald":
    strategy = c()
elif sampling == "Varratio":
    strategy = d()
elif sampling == "Std":
    strategy = e()    
else:
    raise NotImplementedError(f" {sampling} is not implemented")    
    
def find_classes(dir):
    classes = [d for d in os.listdir(dir) if os.path.isdir(os.path.join(dir, d))]
    classes.sort()
    class_to_idx = {classes[i]: i for i in range(len(classes))}
    return classes, class_to_idx

class CheckBoxDataset(torch.utils.data.Dataset):
    """Checkbox dataset."""
    def __init__(self, root_dir: pathlib.Path, transform=None):
        """
        Args:
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """    
        self.paths = []

        self.folder_labels = {"empty": 0, "filled": 1, "no-checkbox": 2}

        for sub_dir in self.folder_labels.keys():
            cur_dir = root_dir / sub_dir
            for file in cur_dir.glob("*.png"):
                self.paths.append(file)
                
        classes, class_to_idx = find_classes(root_dir)  
        self.classes = classes
        self.class_to_idx = class_to_idx
                                  
        self.transform = transform
        assert self.paths

    def __len__(self):
        return len(self.paths)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        img_name = self.paths[idx]    
        label = self.folder_labels[str(self.paths[idx].parent).rsplit("\\",1)[-1]]
        image = PIL.Image.open(img_name)
        
        if image.mode != "RGB":
            image = image.convert("RGB")

        if self.transform:
            image = self.transform(image)    
        else:
            image = transforms.ToTensor()(image)  
        
        return image, label 



train_pipelines = transforms.Compose([transforms.Resize(size=size),
                                      transforms.RandomRotation(degrees=90),
                                      transforms.ColorJitter(brightness=0.9, contrast=0.5, saturation=0.9, hue=0.5),
                                      transforms.RandomGrayscale(),
                                      transforms.RandomHorizontalFlip(),
                                      transforms.RandomVerticalFlip(),
                                      transforms.ToTensor(),
                                      transforms.Normalize(mean=mean,std=std)])

test_pipelines = transforms.Compose([transforms.Resize(size=size),
                                     transforms.ToTensor(),
                                     transforms.Normalize(mean=mean,std=std)])


# In[14]:


train_dataset = CheckBoxDataset(training_data, transform=train_pipelines)
test_dataset = CheckBoxDataset(test_data, transform=test_pipelines)


# In[15]:


train_loader = torch.utils.data.DataLoader(train_dataset,
                                           batch_size= 21580,#Load all data
                                           num_workers=num_workers,
                                           shuffle=shuffle)
test_loader = torch.utils.data.DataLoader(test_dataset,
                                          batch_size= 9911,
                                          num_workers=num_workers)





X, y = next(iter(train_loader))
xtest ,ytest = next(iter(test_loader))




X_train = X.reshape(21580, 3, 64, 64).numpy()
X_test = xtest.reshape(9911, 3, 64, 64).numpy()
y_train = y[:21580].numpy()
y_test = ytest[:9911].numpy()


initial_idx = np.array([],dtype=np.int)
for i in range(3):
    idx = np.random.choice(np.where(y_train==i)[0], size=10, replace=False)
    initial_idx = np.concatenate((initial_idx, idx))

X_initial = X_train[initial_idx]
y_initial = y_train[initial_idx]


# In[19]:


X_pool = np.delete(X_train, initial_idx, axis=0)
y_pool = np.delete(y_train, initial_idx, axis=0)


# In[18]:



device = "cuda" if torch.cuda.is_available() else "cpu"
estimator = NeuralNetClassifier(model,
                                max_epochs=mepochs,
                                batch_size=8,
                                lr= 3e-4,
                                optimizer=torch.optim.Adam,
                                criterion=torch.nn.CrossEntropyLoss,
                                train_split=None,
                                verbose=0,
                                device=device)
perf_hist,modified_learner = active_learning_procedure(strategy,
                                              X_test,
                                              y_test,
                                              X_pool,
                                              y_pool,
                                              X_initial,
                                              y_initial,
                                              estimator,)

print("Saving Accuracy")
np.save(fpath +'Accuracy_'+str(modelname)+str(sampling)+str(expno)+'exp'+str(seed)+'.npy',perf_hist)
print("End of Acquisition")    
